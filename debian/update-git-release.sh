#!/usr/bin/env bash
# Jordan Justen : this file is public domain

THIS_SCRIPT=${BASH_SOURCE[0]}
SCRIPT_DIR="$(cd "$(dirname "$(readlink -f "$THIS_SCRIPT")")" && pwd)"
PKG_DIR="$(cd "$(dirname "$(readlink -f "$SCRIPT_DIR")")" && pwd)"
TARBALL_DIR="$(cd "$(dirname "$(readlink -f "$PKG_DIR")")" && pwd)"

set -e

cd "${PKG_DIR}"

CURRENT_BRANCH=`git branch --show-current`

if [[ "${CURRENT_BRANCH}" != "debian-unstable" ]]; then
    echo "Current branch should be debian-unstable"
    exit 1
fi

if [ -z "$(git status --untracked-files=no --porcelain)" ]; then
    :
else
    echo "Working state of tree should be clean"
    exit 1
fi

TARGET_VERSION=origin/main
#TARGET_VERSION=b8a094494356111860821ac14917c12d39329aa0

# update origin/main branch
if [[ "${TARGET_VERSION}" == "origin/main" ]]; then
    git fetch --no-tags origin main
fi

PACKAGE=piglit
GIT_HASH=`git rev-list ${TARGET_VERSION}~..${TARGET_VERSION}`
ABBREVIATED_HASH=`git log -1 --pretty='%h' ${GIT_HASH}`
export TZ=UTC
DEB_TARBALL_VERSION=`git log -1 --date='format-local:%Y%m%d' --pretty='0.0~git%cd.%h' ${GIT_HASH}`
DEB_VERSION="${DEB_TARBALL_VERSION}-1"
PKG_AND_VERSION="${PACKAGE}_${DEB_TARBALL_VERSION}"
ORIG_TARBALL_BASE="${PKG_AND_VERSION}.orig.tar.xz"
ORIG_TARBALL="${TARBALL_DIR}/${ORIG_TARBALL_BASE}"
OLD_DEB_VERSION=`dpkg-parsechangelog -S Version --file=${SCRIPT_DIR}/changelog`

if [[ "${OLD_DEB_VERSION}" =~ ^(.+)~(.+)[-\.](.+)-(.+)$ ]] ; then
    OLD_DEB_ABBREVIATED_HASH="${BASH_REMATCH[3]}"
    OLD_DEB_HASH=`git rev-list ${OLD_DEB_ABBREVIATED_HASH}~..${OLD_DEB_ABBREVIATED_HASH}`
else
    echo "Didn't match RE with ${OLD_DEB_VERSION}"
    exit 1
fi

UPSTREAM_COMMIT_COUNT=`git rev-list ${OLD_DEB_HASH}..${GIT_HASH} | wc -l`

if [[ ${UPSTREAM_COMMIT_COUNT} -gt 0 ]] ; then
    echo "${UPSTREAM_COMMIT_COUNT} new upstream commit(s)"
else
    echo "No new upstream commits found"
    exit 1
fi

if dpkg --compare-versions "${DEB_VERSION}" le "${OLD_DEB_VERSION}" ; then
    echo "A new version of ${PACKAGE} is *not* available (git=${DEB_VERSION}, deb=${OLD_DEB_VERSION})"
    exit 1
else
    echo "A new version of ${PACKAGE} is available (git=${DEB_VERSION}, deb=${OLD_DEB_VERSION})"
fi

if [ ! -f "${ORIG_TARBALL}" ] ; then
    echo "Generating ${ORIG_TARBALL}"
    git archive --format=tar.xz --prefix="${PKG_AND_VERSION}/" ${GIT_HASH} > "${ORIG_TARBALL}"
else
    echo "Found existing ${ORIG_TARBALL}"
fi

git checkout -q pristine-tar

if pristine-tar list | grep -q "${ORIG_TARBALL_BASE}" ; then
    echo "${ORIG_TARBALL_BASE} already in pristine-tar"
else
    echo "Adding ${ORIG_TARBALL_BASE} to pristine-tar"
    pristine-tar commit ${ORIG_TARBALL} ${GIT_HASH}
fi

git checkout -q "${CURRENT_BRANCH}"

if ! git merge-base --is-ancestor ${GIT_HASH} "${CURRENT_BRANCH}"; then
    git merge --no-edit ${TARGET_VERSION}
fi

{ cat - ${SCRIPT_DIR}/changelog > ${SCRIPT_DIR}/changelog.new; } <<EOF
${PACKAGE} (${DEB_VERSION}) UNRELEASED; urgency=medium

  * Update piglit upstream version to ${ABBREVIATED_HASH}

 -- `git config user.name` <`git config user.email`>  `date -R`

EOF

mv ${SCRIPT_DIR}/changelog.new ${SCRIPT_DIR}/changelog

git add debian/changelog

git commit -asm "d/changelog: Start ${DEB_VERSION} release changelog"
